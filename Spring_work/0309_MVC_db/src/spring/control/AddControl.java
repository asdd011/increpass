package spring.control;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mybatis.dao.DataDAO;
import mybatis.vo.DataVO;

@Controller
public class AddControl {

	@Autowired
	private DataDAO dao;
	
	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value="/add.inc", 
			method=RequestMethod.GET)
	public ModelAndView form(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("form");
		
		return mv;
	}
	
	@RequestMapping(value="/add.inc", 
			method=RequestMethod.POST)
	public ModelAndView add(DataVO vo){
		//파라미터의 이름과 vo의 멤버변수 명이 같은 것들은
		//이미 vo에 저장된 상태다. (ip만 없다.)
		vo.setIp(request.getRemoteAddr());
		
		//DB에 저장
		dao.addData(vo);
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("name", vo.getName());
		mv.setViewName("res");
		
		return mv;
	}
}







