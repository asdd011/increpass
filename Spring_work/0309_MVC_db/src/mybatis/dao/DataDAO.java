package mybatis.dao;

import org.mybatis.spring.SqlSessionTemplate;

import mybatis.vo.DataVO;

public class DataDAO {
	
	public DataDAO(){
		System.out.println("DATADAO생성");
	}

	private SqlSessionTemplate template;

	public void setTemplate(SqlSessionTemplate template) {
		this.template = template;
	}
	
	///  비지니스 로직 ----------------------------
	public void addData(DataVO vo){
		template.insert("data.add", vo);
	}
}







