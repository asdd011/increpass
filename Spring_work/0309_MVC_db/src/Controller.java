

import javax.servlet.annotation.WebServlet;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Servlet implementation class Controller
 */
@WebServlet("*.inc")
public class Controller extends DispatcherServlet {
	// WEB-INF/Controller-servlet.xml을 로드한다.
}
