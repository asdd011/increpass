<%@page import="mybatis.vo.EmpVO"%>
<%@page import="pm.action.TestAction"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	BeanFactory bf =
	new ClassPathXmlApplicationContext("config.xml");

	//empdao가져온다.
	TestAction a = (TestAction)bf.getBean("test");
	
	EmpVO[] ar = a.execute();
%>
	<h1><%=a.getMsg() %></h1>
	<ul>
	<%
		for(EmpVO vo : ar){
	%>
		<li>
			<%=vo.getEmployee_id() %>,
			<%=vo.getFirst_name() %>,
			<%=vo.getJob_id() %>,
			<%=vo.getDepartment_id() %>
		</li>
	<%		
		}
	%>
	</ul>

</body>
</html>