package pm.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import mybatis.dao.EmpDAO;
import mybatis.vo.EmpVO;

public class TestAction {

	//DB를 활용하기 위해 DAO를 준비하자!
	//현재 객체가 생성되기 전에 EmpDAO가 생성되어 있어야 한다
	@Autowired
	private EmpDAO dao;
	
	private String msg;
	
	public String getMsg() {
		return msg;
	}

	@Required
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public EmpVO[] execute(){
		return dao.getAll();
	}
}
