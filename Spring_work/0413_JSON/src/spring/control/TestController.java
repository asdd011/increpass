package spring.control;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {

	
	@RequestMapping("/login")
	public String login(){
		return "login";
	}
	
	@RequestMapping("/test")
	@ResponseBody
	public Map<String, String> aMethod(){
		Map<String, String> map = 
				new HashMap<String, String>();
		
		map.put("v1", "123");
		map.put("name", "마루치");
		map.put("phone", "123-1234");
		
		return map;
	}
	
	@RequestMapping()
	public ModelAndView test(
			@RequestParam(value="age", 
			  required=false, defaultValue="20") int age, String id, String pwd){
		return null;
	}
}








