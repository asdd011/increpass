<%@page import="ex2.MemberVO"%>
<%@page import="java.util.List"%>
<%@page import="ex2.MemberList"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	BeanFactory bf =
	new ClassPathXmlApplicationContext("config2.xml");

	MemberList m = (MemberList)bf.getBean("ml");
	
	List<MemberVO> list = m.getList();
	
	for(MemberVO vo : list){
%>
		<h2><%=vo.getName() %> / <%=vo.getEmail() %></h2>
<%		
	}
%>
</body>
</html>







