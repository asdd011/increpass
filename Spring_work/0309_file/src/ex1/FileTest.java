package ex1;

import java.io.File;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import spring.util.FileUploadUtil;

@Controller
public class FileTest {
	
	@Autowired
	private ServletContext context;
	
	private String uploadPath;
	
	//현재객체가 생성될 때 <property ...>로 값을
	//저장할 수 있도록 setter injection을 지정하도록 한다.
	//그럴려면 setUploadPath()가 있어야 한다.
	public void setUploadPath(String uploadPath){
		this.uploadPath = uploadPath;
	}

	@RequestMapping(value="/write.inc",
			method=RequestMethod.GET)
	public ModelAndView form(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("file_form");
		
		return mv;
	}
	
	@RequestMapping(value="/write.inc",
			method=RequestMethod.POST)
	public ModelAndView write(TestVO vo) throws Exception {
		ModelAndView mv = new ModelAndView();
		
		//파일첨부 여부를 확인한다.
		if(vo.getUpload().getSize() > 0){
			
			//파일들이 저장되는 "/upload"라는 폴더의 경로를
			//절대경로로 만든다.
			String path = context.getRealPath(
					uploadPath);
			
			//첨부된 파일을 vo에서 가져온다.
			MultipartFile upload = vo.getUpload();
			//파일명을 가져온다.
			String filename = upload.getOriginalFilename();
			
			//이미 저장된 파일인지를 판단!!!
			filename = FileUploadUtil.checkSameFileName(
					filename, path);
			
			//파일저장!!!!!!
			upload.transferTo(new File(path, filename));
			
			//나중에 DB에 파일명을 저장하기 위해
			//vo에 파일명을 따로 저장해야 한다.
			vo.setName(filename);
		}
		
		//DAO를 이용하여 DB에 저장하는 부분을
		//이쯤에서 수행한다.
		//dao.writeBbs(vo);
		
		//저장된 파일명을 mv에 저장
		mv.addObject("vo", vo);
		//뷰의 경로 지정
		mv.setViewName("upload_res");
		
		return mv;
	}
}






