<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	ul#menu{
		margin:0;
		width: 200px;
		background-color: #cdcdcd;
		border-top: 2px solid black;
		padding: 0;
		list-style: none;
	}
	ul#menu>li{
		margin:0;
		width: 196px;
		height: 58px;
		border:2px solid black;
		border-top: 0;
		background-color: #00bfff;
		text-align: center;
	}
	ul#menu>li>a{
		display: block;
		width: 100%; /* 196px*/
		height: 100%; /* 58px*/
		font: bold 30px sans-serif;
		color: #000;
		text-decoration: none;
		line-height: 58px;
	}
	/*마우스를 올렸을 때 배경색을 흰색,
	글의 색상은 파랑색 계열, 글꼴크기 32픽셀*/
	ul#menu>li>a:hover{
		background-color: #fff;
		color: #00bfff;
		font-size: 32px;
	}
	div.box{
		display: inline-block;
	/*	border: 1px solid red;  */
		vertical-align: top;
	}
	div.ex1{
		width: 600px;
		height: 500px;
	}
	div.ex1 div{
		display: none;
	}
</style>

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
	function view(idx){
		$(".ex1 div").css("display","none");
		document.forms[0].searchType.value=idx;
		switch(idx){
			case 1:
				$("#s_name").css("display","block");
				document.forms[0].action="searchName.jsp";				
				break;
			case 2:
				$("#s_dept").css("display","block");
				document.forms[0].action="searchDept.jsp";
				break;
			case 3:
				$("#s_job").css("display","block");
				document.forms[0].action="searchJob.jsp";
				break;
		}
		$("#btn").css("display","block");
	}
</script>

</head>
<body>
	<div class="box">
		<ul id="menu">
			<li><a href="all.jsp">전체보기</a></li>
			<li><a href="javascript:view(1)">이름검색</a></li>
			<li><a href="javascript:view(2)">부서검색</a></li>
			<li><a href="javascript:view(3)">직종검색</a></li>
		</ul>
	</div>
	
	<div class="box ex1">
		<form method="post">
		<input type="hidden" name="searchType"/>
		<div id="s_name">
			<label for="name">검색할 이름:</label>
			<input type="text" name="name" id="name"/>
		</div>
		
		<div id="s_dept">
			<label for="deptno">부서코드:</label>
			<input type="text" name="deptno" id="deptno"/>
		</div>
		
		<div id="s_job">
			<label for="jobid">직종코드:</label>
			<input type="text" name="jobid" id="jobid"/>
		</div>
		
		<div id="btn">
			<input type="submit" value="검색"/>
		</div>
		</form>
	</div>
</body>
</html>




