<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="javax.sql.DataSource"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	BeanFactory bf =
	new ClassPathXmlApplicationContext("config.xml");

	DataSource ds = (DataSource)bf.getBean("ds");
	
	//얻어낸 DS를 통하여 java.sql.Connection을 얻어낸다.
	Connection con = ds.getConnection();
	
	//구문객체
	PreparedStatement ps = con.prepareStatement(
		"SELECT * FROM employees WHERE department_id=?");
	
	//바인드변수(?)를 채워준다.
	ps.setString(1, "80");
	
	//구문객체 실행 및 결과얻기
	ResultSet rs = ps.executeQuery();
	
	//결과처리
	while(rs.next()){
		StringBuffer sbuf = new StringBuffer();
		
		sbuf.append(rs.getString("employee_id"));
		sbuf.append(", ");
		sbuf.append(rs.getString("first_name"));
		sbuf.append(", ");
		sbuf.append(rs.getString("job_id"));
		sbuf.append(", ");
		sbuf.append(rs.getString("department_id"));
		sbuf.append("<br/>");
%>
		<%=sbuf.toString() %>
<%		
	}//while의 끝
	
	rs.close();
	ps.close();
	con.close();
%>
</body>
</html>






