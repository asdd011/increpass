package ex1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class TestObj {
	@Autowired
	@Qualifier("vo2")
	private TestVO tvo;
	
	
	public String getMsg(){
		return tvo.getMsg();
	}
}
