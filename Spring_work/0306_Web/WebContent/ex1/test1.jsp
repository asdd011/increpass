<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="ex1.TestObj"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
//	BeanFactory bf = 
//	new ClassPathXmlApplicationContext("config.xml");

//다음을 수행하기에 앞서 Listener가 구동되어 있어야 함!
	ApplicationContext bf =
	WebApplicationContextUtils.getWebApplicationContext(
			request.getServletContext());

//	TestObj obj = (TestObj)bf.getBean("obj");
	TestObj obj = bf.getBean(TestObj.class);
%>
	<h1><%=obj.getMsg() %></h1>
</body>
</html>






