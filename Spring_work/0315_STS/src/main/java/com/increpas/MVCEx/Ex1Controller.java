package com.increpas.MVCEx;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Ex1Controller {
	@RequestMapping("/ex1.inc")
	public String ex1(Model m){
		m.addAttribute("msg", "반갑습니다.");
		
		return "ex1";
	}
}
