package com.increpas.MVCEx.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {

	@RequestMapping("/ex2.inc")
	public ModelAndView exe(){
		ModelAndView mv = new ModelAndView();
		mv.addObject("str", "nice meet u~");
		mv.setViewName("ex2");
		
		return mv;
	}
}
