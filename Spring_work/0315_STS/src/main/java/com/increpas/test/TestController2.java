package com.increpas.test;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController2 {

	@RequestMapping("/ex3.inc")
	public String test(Model m){
		m.addAttribute("data", "Michael");
		
		return "ex3";
	}
}
