package ex1;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class Test3 extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {

		// List구조 생성
		List<String> list = new ArrayList<>();
		list.add("항목1");
		list.add("항목2");
		list.add("항목3");
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("list", list);
		mv.setViewName("ex1/list");
		
		return mv;
	}

}
