package ex2;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class Test4 extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		// map구조 생성
		Map<String, String> map = new HashMap<>();
		map.put("msg", "스프링MVC");
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("map", map);
		mv.setViewName("ex2/map");
		return mv;
	}

}
