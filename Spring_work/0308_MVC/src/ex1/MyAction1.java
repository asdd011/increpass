package ex1;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MyAction1 {
	
	private String msg;
	private int value = 0;

	@RequestMapping("/t1")
	public ModelAndView test1(){
		ModelAndView mv = new ModelAndView();
		//JSP에서 표현할 값이 있다면 mv에 저장한다.
		mv.addObject("v1", "스프링MVC");
		value++;
		
		return mv;
	}
	
	@RequestMapping("/t2")
	public Map<String, Integer> test2(){
		value++;
		Map<String, Integer> map = new HashMap<>();
		map.put("data", value);
		
		return map;
	}
}
