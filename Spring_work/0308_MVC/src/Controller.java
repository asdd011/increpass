

import javax.servlet.annotation.WebServlet;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Servlet implementation class Controller
 */
@WebServlet("*.inc")
public class Controller extends DispatcherServlet {
	// 현재 컨트롤러가 WEB-INF/Controller-servlet.xml문서와
	// 연동된다.

}
