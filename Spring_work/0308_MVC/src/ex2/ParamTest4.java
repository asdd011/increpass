package ex2;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ParamTest4 {

	@Autowired
	private HttpServletRequest request;

	@RequestMapping(value="/param4",method=RequestMethod.GET)
	public ModelAndView exe(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("param4_form");
		
		return mv;
	}
	
	@RequestMapping(value="/param4", 
			method=RequestMethod.POST)
	public ModelAndView execute(){
		//파라미터 값들 받기
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		
		//받은 값들을 하나의 객체로 만든다.
		TestVO vo = new TestVO();
		vo.setName(name);
		vo.setEmail(email);
		vo.setPhone(phone);
		
		ModelAndView mv = new ModelAndView();
		//위에서 만든 객체를 mv에 저장
		mv.addObject("vo", vo);
		mv.setViewName("param4_res");//뷰의 이름 지정
		
		return mv;
	}
}









