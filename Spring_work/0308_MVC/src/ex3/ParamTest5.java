package ex3;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ParamTest5 {

	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value="/pa5", 
			method=RequestMethod.POST)
	public ModelAndView execute(DataVO vo){
				
		ModelAndView mv = new ModelAndView();
		//위에서 만든 객체를 mv에 저장
		mv.addObject("vo", vo);
		mv.setViewName("param5_res");//뷰의 이름 지정
		
		return mv;
	}
}









