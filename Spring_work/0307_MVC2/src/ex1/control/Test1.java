package ex1.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import mybatis.dao.EmpDAO;
import mybatis.vo.EmpVO;

public class Test1 extends AbstractController {

	private EmpDAO e_dao;
	
	public void setE_dao(EmpDAO dao){
		this.e_dao = dao;
	}
	
	@Override
	protected ModelAndView handleRequestInternal(
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		// EmpDAO를 활용하여 EmpVO목록을 가져온다.
		EmpVO[] ar = e_dao.getAll();
		
		ModelAndView mv = new ModelAndView();
		//ar을 jsp에서 표현하기 위해 mv에 저장한다.
		mv.addObject("list", ar);
		mv.setViewName("ex1/list");
		
		return mv;
	}

}
