var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');

var app = express();

app.listen(55555);

app.use(bodyParser.urlencoded({extend:false}));

app.get('/', function(request, response){
  var name = request.query.name;
  console.log(name);
  response.type('text/html');
  response.send('<h1>메인페이지</h1>'+name+'님 환영합니다.');
});

app.get('/login', function(request, response){
  response.type('text/html');
  fs.readFile('./html/login.html', 'utf-8',
    function(err, data){
      response.send(data);
  });
});
app.post('/login', function(request, response){
  response.type('text/html');
  var id = request.body.id;
  var password = request.body.password;
  console.log(id, password);
  response.redirect('/');
});

app.get('/user/:id', function(request, response){
  var id = request.params.id;
  var users = [
    {name:'flynn', job:'teacher'},
    {name:'이성계', job:'king'}
  ];
  response.type('text/html');
  response.send('<h1>이름:'+users[id].name+' 직업:'
            +users[id].job+'</h1>');
});
