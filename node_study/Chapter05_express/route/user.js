var express = require('express');
var router = express.Router();

router.get('/:id',
  function(request, response){
    var id = request.params.id;
    var users = [
      {name:'flynn', job:'teacher'},
      {name:'정도전', job:'개국공신'}
    ];
    response.type('text/html');
    response.send('<h1>'+users[id].name+"/"+
              users[id].job+'</h1>');
});

module.exports = router;
