var express = require('express');
var session = require('express-session');
var fs = require('fs');
var bodyParser = require('body-parser');

var app = express();

app.listen(55555);

app.use(bodyParser.urlencoded(
  {extend:false}
));

app.use(session({
  secret: 'hello',
  resave: false,
  saveUninitialized: true
}));

app.get('/', function(request, response){
  var name = request.query.name;
  console.log(name);
  response.type('text/html');
  response.send('<h1>메인페이지</h1>'+name+'님 환영합니다.');
});

app.get('/login', function(request, response){
  //이미 로그인 되어있다면?
  console.log(request.session.USER);
  if(request.session.USER){
    console.log('이미 로그인 되어 있습니다.');
    response.redirect('/');
    return;
  }
  response.type('text/html');
  fs.readFile('./html/login.html', 'utf-8',
    function(err, data){
      response.send(data);
  });
});
app.post('/login', function(request, response){

  //정상적인 로그인 처리
  response.type('text/html');
  var id = request.body.id;
  var password = request.body.password;

  var user = {userId:id, userPassword:password}
  request.session.USER = user;
  console.log(id, password);
  response.redirect('/');
});
