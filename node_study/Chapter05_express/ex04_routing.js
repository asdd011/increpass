var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.listen(55555);

app.use(bodyParser.urlencoded(
  {extend:false}
));

app.get('/', function(request, response){
  response.type('text/html');
  response.send('<h1>메인페이지</h1>');
})

app.use('/user', require('./route/user'));
app.use('/login', require('./route/login'));
