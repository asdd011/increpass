var http = require('http');
var fs = require('fs');
var socketio = require('socket.io');

var server = http.createServer(
    function(request, response){
      fs.readFile('./html/client.html',
                  'utf-8',
      function(err, data){
        response.writeHead(
          200, {'Content-Type':'text/html'});
        response.end(data);
      });
    });
server.listen(55555);

var io = socketio.listen(server);
io.sockets.on('connection', function(socket){
  socket.on('broadcast', function(data){
    console.log(data);
    io.sockets.emit('receive', data);
  });
});
