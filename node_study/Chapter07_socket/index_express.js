var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var fs = require('fs');
var socketio = require('socket.io');

app.get('/', function(request, response){
  fs.readFile('./html/client_express.html',
              'utf-8',
      function(err, data){
            response.type('text/html');
            response.send(data);
  });
});

var roomName = 'client';
var users = [];

server.listen(55555);

var io = socketio.listen(server);

io.sockets.on('connection',
  function(socket){
    socket.on('join', function(name){
      users.push({name:name, id:socket.id});
      socket.emit('view_member', users);
      io.sockets.in(roomName).emit('update_member',
        {name:name, id:socket.id}
      );
      socket.join(roomName);
    });
    socket.on('sendTo', function(data){
      console.log(data);
      io.sockets.in(roomName).sockets[data.to].emit(
        'receive', {name:data.name, msg:data.msg}
      );
      socket.emit('receive',
      {name:data.name, msg:data.msg});
    });
  });
