var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.listen(55555);

app.use(bodyParser.urlencoded({extend:false}));

app.get('/', function(request, response){
  response.type('text/html');
  response.send('<h1>메인 페이지</h1>');
});

app.use('/musician', require('./route/musician'));
