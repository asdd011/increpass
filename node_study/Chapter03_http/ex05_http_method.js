var http = require('http');
var fs = require('fs');
var url = require('url');
var querystring = require('querystring');

var server = http.createServer(function(request, response){
  var pathname = url.parse(request.url).pathname;
  var query = url.parse(request.url, true).query;
  var name = query.name;
  response.writeHead(200, {'Content-Type':'text/html'});
  if(pathname == '/'){
    fs.readFile('./html/main.html','utf-8', function(err, data){
      response.end(data);
    });
  }else if (pathname == '/login' && request.method=="GET") {
    fs.readFile('./html/login.html','utf-8', function(err, data){
      response.end(data);
    });
  }else if (pathname == '/login' && request.method=="POST"){
    request.on("data", function(data){
      var params = querystring.parse(data.toString());
      var id = params.id;
      var password = params.password;
      console.log(id, password);
      response.writeHead(302,{'Location':'/'});
      response.end();
    });
  }else{
    response.end('<h1>Page Not Found!!</h1>');
  }
});

server.listen(55555, function(){
  console.log('Server running at http://localhost:55555');
});
