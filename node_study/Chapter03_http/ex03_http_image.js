var http = require('http');
var fs = require('fs');

var server = http.createServer(function(request, response){
  fs.readFile("./image/Penguins.jpg", function(err, data){
    response.writeHead(200, {'Content-Type':'image/jpg'});
    response.end(data);
  });
});

server.listen(55555, function(){
  console.log('Server running at http://localhost:55555');
});
