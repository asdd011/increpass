var http = require('http');
var fs = require('fs');

var server = http.createServer(function(request, response){
  fs.readFile("./html/hello.html", 'utf-8', function(err, data){
    response.writeHead(200, {'Content-Type':'text/html'});
    response.end(data);
  });
});

server.listen(55555, function(){
  console.log('Server running at http://localhost:55555');
});
