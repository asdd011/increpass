var http = require('http');
var fs = require('fs');
var url = require('url');
var querystring = require('querystring');
var ejs = require('ejs');

var server = http.createServer(function(request, response){
  var pathname = url.parse(request.url).pathname;
  response.writeHead(200,{'Content-Type':'text/html; charset="utf-8'});
  if(pathname == '/'){
    response.end('<h1>메인페이지</h1>');
  }else if(pathname == '/user'){
    fs.readFile('./ejs/user.ejs','utf-8', function(err, data){
      var page = ejs.render(data, {'title':'MyPage','name':'Flynn'});
      response.end(page);
    });
  }
});

server.listen(55555,function(){
  console.log('Sever running at http://localhost:55555');
});
