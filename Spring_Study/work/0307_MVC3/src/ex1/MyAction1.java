package ex1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MyAction1 {
	
	public MyAction1(){
		System.out.println("MyAction1생성");
	}

	@RequestMapping("/ex1/test")
	public ModelAndView test(){
		ModelAndView mv = new ModelAndView();
		mv.addObject("msg", "연습입니다!!");
//		mv.setViewName("ex1/test");
		
		return mv;
	}
	
	@RequestMapping("/ex1/test2")
	public ModelAndView test2(){
		ModelAndView mv = new ModelAndView();
		mv.addObject("avg", 33);
		
		return mv;
	}
	
	
	
	
}
