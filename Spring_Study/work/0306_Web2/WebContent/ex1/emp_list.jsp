<%@page import="mybatis.vo.EmpVO"%>
<%@page import="mybatis.dao.EmpDAO"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	ApplicationContext ctx =
	WebApplicationContextUtils.getWebApplicationContext(
			request.getServletContext());

	EmpDAO e_dao = ctx.getBean(EmpDAO.class);
	
	EmpVO[] ar = e_dao.getAll();
%>
	<ul>
	<%
		for(EmpVO vo : ar){
	%>
		<li>
			<%=vo.getEmployee_id() %>,
			<%=vo.getFirst_name() %>, 
			<%=vo.getJob_id() %>,
			<%=vo.getDepartment_id() %>
		</li>
	<%		
		}
	%>
	</ul>
</body>
</html>






