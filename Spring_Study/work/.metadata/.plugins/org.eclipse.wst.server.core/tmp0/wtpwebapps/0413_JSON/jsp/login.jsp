<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	input[type=text],input[type=password]{
		width: 120px;
	}
	.box{
		width: 220px;
		height: 140px;
		border: 1px solid black;
	}
	#name_area{
		color: lightblue;
		font-weight: bold;
	}
</style>
<script src="http://code.jquery.com/jquery-1.9.0rc1.js"></script>
<script type="text/javascript">
//$( document ).ready(function() {
$(function(){

    // 현재문서에서 id가 login_success_box를 검색하여 
    // display를 none으로 설정하자!
    //document.getElementById("login_success_box").style.display = "none";
	$("#login_success_box").css("display","none");
});

function login(){
	var url = "test.inc";
//	var param = "id="+$("#id").val() + "&pwd="+$("#pw").val();
	 // id=test1&pwd=1234
	$.ajax({
		type: "POST",
		url: url,
//		data: param,
		success: function(res){
			if(res != "null"){
			//	var obj = JSON.parse(res);
				
				//alert(obj.name);
				$("#name_area").text(res.name);
				$("#login_form_box").css("display","none");
				$("#login_success_box").css("display","block");
			}else
				alert("아이디나 비번이 다릅니다.");
		},
		error: function(req, err, ex){
			alert(req.status + ": " +req.statusText);
		}
	});
	
}


</script>
</head>
<body>

	<div id="login_form_box" class="box">
		<form>
			아이디:<input type="text" id="id" name="id"/><br/>
			비밀번호:<input type="password" id="pw" name="pwd"/><br/>
			<input type="button" value="로그인" 
			 onclick="login()"/>
		</form>
	</div>
	
	<div id="login_success_box" class="box">
		<span id="name_area"></span>님 환영!
	</div>
</body>
</html>



