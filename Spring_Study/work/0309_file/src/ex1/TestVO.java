package ex1;

import org.springframework.web.multipart.MultipartFile;

public class TestVO {
	
	private String name;
	
	//스프링환경에서 첨부파일을 받을 때는
	//반드시 MultipartFile로 선언해야 한다.
	// (java.io.File가 아니다.)
	private MultipartFile upload;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MultipartFile getUpload() {
		return upload;
	}

	public void setUpload(MultipartFile upload) {
		this.upload = upload;
	}
}
