package ex1;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyAction2 {

	@RequestMapping("/t3")
	public Model ex1(){
		Model m = new ExtendedModelMap();
		
		//모델객체에 값을 저장
		m.addAttribute("title", "스프링~~ 봄이다!");
		return m;
	}
}
