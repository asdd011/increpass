package ex1;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ParamTest3 {

	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(value="/param3", 
			method=RequestMethod.GET)
	public ModelAndView exe(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("param3_form");
		
		return mv;
	}
	
	@RequestMapping(value="/param3", 
			method=RequestMethod.POST)
	public ModelAndView exe2(){
		
		String name = request.getParameter("name");
		String msg = name+"님 환영!";
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("msg", msg);
		mv.setViewName("param3_res");
		
		return mv;
	}
}









