package ex1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ParamTest {
	
	
	@RequestMapping("/param1")
	public Model test(HttpServletRequest request,
			HttpServletResponse response){
		//파라미터 받기
		String type = request.getParameter("type");
		Model m = new ExtendedModelMap();
		m.addAttribute("t1", type);
		
		return m;
	}
}





