package ex1;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

/**
 * Application Lifecycle Listener implementation class MyListener
 *
 */
@WebListener
public class MyListener extends ContextLoaderListener 
implements ServletContextListener {
	// 부모인 ContextLoaderListener에 의해서
	// WEB-INF/applicationContext.xml이 로드된다.
	//현재 Listener는 프로젝트가 구동될 때 한번 움직인다.
}
