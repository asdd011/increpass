package ex1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class Test1 extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		// 요청에 따른 구현력을 수행하거나
		// 파라미터에 따른 뷰를 설정할 수 있다.
		
		ModelAndView mv = new ModelAndView();
		//뷰의 경로를 지정한다.
		mv.setViewName("ex1/hello");
		return mv;
	}

}
