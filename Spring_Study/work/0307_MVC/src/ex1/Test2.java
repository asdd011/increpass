package ex1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class Test2 extends AbstractController {

	@Override
	protected ModelAndView handleRequestInternal(
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {

		ModelAndView mv = new ModelAndView();
		mv.addObject("v1", "Michael");
		mv.addObject("v2", "INCREPAS");
		mv.setViewName("ex1/hi");
		
		request.setAttribute("v3", "반갑습니다.");
		
		return mv;
	}

}
