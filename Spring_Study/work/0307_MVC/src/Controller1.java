

import javax.servlet.annotation.WebServlet;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Servlet implementation class Controller1
 */
@WebServlet("*.inc")
public class Controller1 extends DispatcherServlet {
	// WEB-INF에 있는 xml문서들 중에서
	// 현재 서블릿의 이름으로 시작하는
	// Controller1-servlet.xml문서를 자동으로 로드한다.
}
