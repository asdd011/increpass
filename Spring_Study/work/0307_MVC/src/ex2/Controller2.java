package ex2;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Servlet implementation class Controller2
 */
@WebServlet("*.do")
public class Controller2 extends DispatcherServlet {
	
	//현재 컨트롤러가 ex2패키지에 있으므로
	//전체경로가 ex2.Controller2가 된다. 그래서
	//xml문서의 이름이
	// ex2.Controller2-servlet.xml 로 설정하여
	// WEB-INF에 만들어져야 한다.
}
