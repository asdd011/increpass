package mybatis.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import mybatis.vo.EmpVO;

public class EmpDAO {
	
	public EmpDAO() {
		System.out.println("EmpDAO 생성!");
	}
	
	@Autowired
	private SqlSessionTemplate template;	


	public EmpVO[] getAll(){		
		EmpVO[] ar = null;
		List<EmpVO> list = template.selectList("emp.all");
		
		if(list != null && list.size() > 0){
			ar = new EmpVO[list.size()];
			
			list.toArray(ar);
		}
		return ar;
	}
	

	//이름 검색
	public EmpVO[] getSearch(String type, String value){
		
		Map<String, String> map = new HashMap<>();
		map.put("searchType", type);
		map.put("searchValue", value);
		
		EmpVO[] ar = null;
		List<EmpVO> list = template.selectList("emp.search",map);
		
		if(list != null && list.size() > 0){
			ar = new EmpVO[list.size()];
			
			list.toArray(ar);
		}
		return ar;
	}
}







