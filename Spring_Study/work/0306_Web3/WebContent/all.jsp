<%@page import="mybatis.vo.EmpVO"%>
<%@page import="mybatis.dao.EmpDAO"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	#table{
		border-collapse: collapse;
		border: 1px solid black;
		width: 400px;
	}
	#table>thead>tr{
		background-color: #cdcdcd;
		height: 35px;
	}
	#table th, #table td{
		border: 1px solid black;
	}
</style>
</head>
<body>
<%
	ApplicationContext ctx = 
	WebApplicationContextUtils.getWebApplicationContext(
			request.getServletContext());

	//EmpDAO검색
	EmpDAO e_dao = ctx.getBean(EmpDAO.class);
	
	EmpVO[] ar = e_dao.getAll();
%>
	<h1>전체보기</h1>
	<table id="table">
		
		<thead>
			<tr>
				<th>사번</th>
				<th>이름</th>
				<th>직종</th>
				<th>부서</th>
			</tr>
		</thead>
		<tbody>
		<%
			for(EmpVO vo : ar){
		%>
			<tr>
				<td><%=vo.getEmployee_id() %></td>
				<td><%=vo.getFirst_name() %></td>
				<td><%=vo.getJob_id() %></td>
				<td><%=vo.getDepartment_id() %></td>
			</tr>
		<%		
			}
		%>
		</tbody>
	</table>
</body>
</html>




