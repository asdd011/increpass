package ex2;

import java.util.List;

public class MemberList {
	private List<MemberVO> list;

	public List<MemberVO> getList() {
		return list;
	}

	public void setList(List<MemberVO> list) {
		this.list = list;
	}
}
