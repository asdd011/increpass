package ex1;

public class Test3 {
	private String msg;
	private int value;
	// 아래와 같이 생성자가 존재하면
	// 정의된 생성자 외에 다른 생성자로 객체를
	// 생성할 수 없다.
	public Test3(String msg, int value) {
		super();
		this.msg = msg;
		this.value = value;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
}
