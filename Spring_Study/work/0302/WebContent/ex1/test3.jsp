<%@page import="ex1.Test3"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	BeanFactory bf = 
	new ClassPathXmlApplicationContext("config.xml");

	Test3 t = (Test3)bf.getBean("t3");
%>
	<h1><%=t.getMsg() %> / <%=t.getValue() %></h1>
</body>
</html>





