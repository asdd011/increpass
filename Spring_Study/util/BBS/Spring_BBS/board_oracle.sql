CREATE SEQUENCE bbs2_seq
increment by 1
start with 1
nomaxvalue
nocycle
cache 10;

CREATE SEQUENCE bbs2_group
increment by 1
start with 1
nomaxvalue
nocycle
cache 10;

CREATE TABLE bbs2(
	seq NUMBER,
	writer VARCHAR2(10) NOT NULL,
	title VARCHAR2(100) NOT NULL,
	content CLOB,
	pwd VARCHAR2(20) NOT NULL,
	hit NUMBER NOT NULL,
	groups NUMBER NOT NULL,
	step NUMBER NOT NULL,
	lev NUMBER NOT NULL,
	bname VARCHAR2(10) NOT NULL,
	regdate date NOT NULL,
	uploadFileName VARCHAR2(50),
	ip VARCHAR2(15),
	status NUMBER(1),
	CONSTRAINT bbs2_pk PRIMARY KEY (seq)
);


INSERT INTO bbs2(seq,writer,title,pwd,hit,groups,step,lev,
		bname,regdate,status) 
values(bbs2_seq.NEXTVAL,'마루치','연습입니다.','1111',0,
	bbs2_group.NEXTVAL,0,0,'BBS',sysdate,0);





