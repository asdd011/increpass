<%@page import="ex1.Test2"%>
<%@page import="ex1.Test1"%>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.beans.factory.BeanFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	BeanFactory bf =
	new ClassPathXmlApplicationContext("config.xml");

	//필요한 빈객체를 가져온다.
	Test1 tt = (Test1)bf.getBean("t1");
	//tt.setMsg("DI연습");
	
	Test2 t2 = (Test2)bf.getBean("t2");
%>
	<h1><%=tt.getMsg() %></h1>
	<h1><%=t2.getStr() %> / <%=t2.getValue()+1 %></h1>
</body>
</html>



