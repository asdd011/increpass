package ex1;

public class Test4 {
	private String str;
	private int value;
	private boolean check;
	
	public Test4(String str, int value, boolean check) {
		super();
		this.str = str;
		this.value = value;
		this.check = check;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public boolean isCheck() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
}
